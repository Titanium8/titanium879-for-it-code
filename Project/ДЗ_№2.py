"""ДЗ_№2"""
class Counter:
    """Счетчик с возвратом значений +1 и -1 соответственно"""
    def __init__(self, initial_value):
        self.value = initial_value

    def inc(self):
        self.value += 1
        return self.value

    def dec(self):
        self.value -= 1
        return self.value


class ReverseCounter(Counter):
    """"Обратный счетчик с возвратом значений"""
    def inc(self):
        self.value -= 1
        return self.value

    def dec(self):
        self.value += 1
        return self.value


def get_counter(number):
    """Функция с условием возврата значений"""
    if number >= 0:
        counter = Counter(number)
    else:
        counter = ReverseCounter(number)
    return counter
