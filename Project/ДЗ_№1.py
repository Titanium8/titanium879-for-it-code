"""Доммашнее задание №1"""
"""Решение первой задачи"""


def func_1(a, b, c):
    q = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря']
    i = b
    b = q[i-1]
    return(a, b, c, 'года')

result = func_1(a=19, b=11, c=2023)
print(result)


""""Решение второй задачи"""


def func_2(names_tuple):
    e = {}
    for name in names_tuple:
        if name in e:
            e[name] += 1
        else:
            e[name] = 1
    return e

names_tuple = ('Олег', 'Игорь', 'Анна', 'Анна', 'Игорь', 'Анна', 'Василий')
result = func_2(names_tuple)
print(result)


""""Решение третьей задачи"""


def func_3(g):
    if not g:
        return 'Нет данных'

    first_name = g.get('first_name', '')
    last_name = g.get('last_name', '')
    middle_name = g.get('middle_name', '')

    if last_name and first_name and middle_name:
        return f'{last_name} {first_name} {middle_name}'
    elif last_name and first_name:
        return f'{last_name} {first_name}'
    elif last_name and middle_name:
        return f'{last_name}'
    elif first_name and middle_name:
        return f'{first_name}{middle_name}'
    elif last_name:
        return last_name
    elif first_name:
        return first_name
    elif middle_name:
        return 'Нет данных'
    else:
        return 'Нет данных'

g = {'first_name': 'Иван', 'last_name': 'Иванов', 'middle_name': 'Иванович'}
result = func_3(g)
print(result)


""""Решение четвертой задачи"""


def func_4(j):
    k = 0
    for i in range(2, j // 2+1):
        if (j % i == 0):
            k += 1
    if (k <= 0):
        return True
    else:
        return False

result = func_4(j = 3)
print(result)


""""Решение пятой задачи"""


def func_5(*args):
    l = []
    for arg in args:
        if isinstance(arg, int) and arg not in l:
            l.append(arg)
    l.sort()
    return l

result = func_5(1, '2', 'text', 42, None, None, None, 15, True, 1, 1)
print(result)
