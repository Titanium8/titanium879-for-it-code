from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, FormView, CreateView
from core import models, forms


class Homepage(TemplateView):
    template_name = 'core/mainpage.html'
    extra_context = {'title': 'Портал Мед Услуг: Вход'}


class Info(TemplateView):
    template_name = 'core/info.html'
    extra_context = {'title': 'Информация о проекте'}


class Recovery(TemplateView):
    template_name = 'core/recovery.html'
    extra_context = {'title': 'Восстановление пароля'}


class Doctors(ListView):
    template_name = 'core/Doctors.html'
    extra_context = {'title': 'Доктора'}
    context_object_name = 'Doctors'
    model = models.Doctor


class Clients(ListView):
    template_name = 'core/Clients.html'
    extra_context = {'title': 'Клиенты'}
    context_object_name = 'Clients'
    model = models.Client


class Assistants(ListView):
    template_name = 'core/Assistants.html'
    extra_context = {'title': 'Помощники'}
    context_object_name = 'Assistants'
    model = models.Assistant


class Redzones(ListView):
    template_name = 'core/Redzones.html'
    extra_context = {'title': 'Карантийная зона'}
    context_object_name = 'Redzones'
    model = models.Redzone


class AddCabinets(FormView):
    template_name = 'core/AddCabinets.html'
    extra_context = {'title': 'Добавление кабинета'}
    form_class = forms.Redzones
    success_url = reverse_lazy('redzones')

    def form_valid(self, form):
        form.save()
        return redirect(self.get_success_url())


class AddDoctors(CreateView):
    model = models.Doctor
    fields = '__all__'
    extra_context = {'title': 'Добавление доктора'}
    template_name = 'core/AddDoctors.html'
    success_url = reverse_lazy('doctors')


class AddClients(CreateView):
    model = models.Client
    fields = '__all__'
    extra_context = {'title': 'Добавление клиента'}
    template_name = 'core/AddClients.html'
    success_url = reverse_lazy('clients')


class AddAssistants(CreateView):
    model = models.Assistant
    fields = '__all__'
    extra_context = {'title': 'Добавление ассистента'}
    template_name = 'core/AddAssistants.html'
    success_url = reverse_lazy('assistants')
