from django.db import models


class Doctor(models.Model):
    last_name = models.CharField('фамилия', max_length=30)
    first_name = models.CharField('имя', max_length=30)
    middle_name = models.CharField('отчество', max_length=30, blank=True)
    birthday = models.DateField('дата рождения', null=True, blank=True)
    gender = models.CharField('пол', max_length=50, blank=True)
    doctors_specialization = models.CharField('специализация врача', max_length=255)


class Client(models.Model):
    doctor = models.ManyToManyField(Doctor, verbose_name='доктор', related_name='clients')
    last_name = models.CharField('фамилия', max_length=30)
    first_name = models.CharField('имя', max_length=30)
    middle_name = models.CharField('отчество', max_length=30, blank=True)
    birthday = models.DateField('дата рождения', null=True, blank=True)
    gender = models.CharField('пол', max_length=50, blank=True)
    diagnosis = models.CharField('диагноз', max_length=50, blank=True)
    passport_data = models.CharField('паспортные данные', max_length=30, blank=True)
    email = models.EmailField('адрес электронной почты', max_length=255, blank=True)
    growth = models.IntegerField('рост', null=True, blank=True)
    weight = models.IntegerField('вес', null=True, blank=True)
    blood_type = models.CharField('группа крови', max_length=50, blank=True)
    rhesus_factor = models.CharField('резус-фактор', max_length=30, blank=True)
    list_of_transferred_diseases = models.TextField('список перенесенных заболеваний', blank=True)
    list_of_transferred_operations = models.TextField('список перенесенных операций', blank=True)
    count_of_hospitalizations = models.PositiveIntegerField('количество госпитализаций', null=True, blank=True)
    date_of_hospitalization = models.CharField('дата госпитализации', max_length=255, blank=True)
    hospital_address = models.CharField('номер больницы-госпитализации', max_length=255, blank=True)
    reason_for_hospitalization = models.CharField('причина госпитализации', max_length=255, blank=True)
    general_blood_test = models.TextField('общий анализ крови', max_length=255, blank=True)
    biochemical_blood_analysis = models.TextField('биохимический анализ крови', max_length=255, blank=True)
    general_urine_analysis = models.TextField('общий анализ мочи', max_length=255, blank=True)
    blood_pressure = models.CharField('артериальное давление', max_length=255, blank=True)
    decoding_X_ray = models.TextField('расшифровка рентгенограммы', blank=True)
    decoding_of_fgds = models.TextField('расширфока фиброгастродуоденоскопии', blank=True)
    decoding_mri = models.TextField('расшифровка мрт', blank=True)
    decoding_electrocardiography = models.TextField('расширофка экг', blank=True)


class Assistant(models.Model):
    doctor = models.ForeignKey(Doctor, verbose_name='доктор', related_name='doctorsforpers', on_delete=models.PROTECT)
    last_name = models.CharField('фамилия', max_length=30)
    first_name = models.CharField('имя', max_length=30)
    middle_name = models.CharField('отчество', max_length=30, blank=True)
    birthday = models.DateField('дата рождения', null=True, blank=True)
    gender = models.CharField('пол', max_length=50, blank=True)
    assistant_specialization = models.CharField('специализация помощника', max_length=255, blank=True)


"""за каждым врачом закреплен хотя бы один помощник (медбрат, медсестра и тд)"""


class Redzone(models.Model):
    client = models.ManyToManyField(Client, verbose_name='клиент', related_name='redzones')
    cabinet_number = models.CharField('номер кабинета в карантийной зоне', max_length=50)
    date_admission = models.DateTimeField('дата поступления больного в карантийную зону', null=True, blank=True)
