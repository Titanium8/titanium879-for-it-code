from django.utils import timezone
from django import forms
from core import models


class Redzones(forms.ModelForm):
    class Meta:
        model = models.Redzone
        fields = '__all__'

    def clean_date_admission(self):
        now = timezone.now()
        if self.cleaned_data['date_admission'] > now:
            raise forms.ValidationError('Как вы хотите добавить больного в красную зону заранее...')
        return self.cleaned_data['date_admission']
