# Generated by Django 4.2.7 on 2023-12-04 15:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_assistant_birthday_client_birthday'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assistant',
            name='birthday',
            field=models.DateField(verbose_name='дата рождения'),
        ),
        migrations.AlterField(
            model_name='client',
            name='birthday',
            field=models.DateField(verbose_name='дата рождения'),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='birthday',
            field=models.DateField(verbose_name='дата рождения'),
        ),
    ]
