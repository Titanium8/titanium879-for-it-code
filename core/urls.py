from django.urls import path, include

from core.views import Info, Doctors, Clients, Assistants, Redzones, AddCabinets, AddDoctors, AddClients, AddAssistants

urlpatterns = [
    path('', include('django.contrib.auth.urls'), name='homepage'),
    path('info/', Info.as_view(), name='info'),
    path('Doctors/', Doctors.as_view(), name='doctors'),
    path('Clients/', Clients.as_view(), name='clients'),
    path('Assistants/', Assistants.as_view(), name='assistants'),
    path('Redzones/', Redzones.as_view(), name='redzones'),
    path('Redzones/add/', AddCabinets.as_view(), name='addcabinets'),
    path('Doctors/add/', AddDoctors.as_view(), name='adddoctors'),
    path('Clients/add/', AddClients.as_view(), name='addclients'),
    path('Assistants/add/', AddAssistants.as_view(), name='addassistants')
]
