from django.contrib import admin

from core.models import Client, Doctor, Assistant, Redzone


@admin.register(Client)
class Client(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'diagnosis')


@admin.register(Doctor)
class Doctor(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'doctors_specialization')


@admin.register(Assistant)
class Assistant(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'assistant_specialization')


@admin.register(Redzone)
class Redzone(admin.ModelAdmin):
    list_display = ('cabinet_number', 'date_admission')
